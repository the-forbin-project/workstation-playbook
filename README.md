# macOS workstation playbook

An Ansible playbook to provision macOS workstations.

## Usage

1. Edit `variables.yml` to specify the desired hostname, Homebrew repositories
   to tap, formulae and casks to install, services to start and register to
   launch at login, and Python version to install.
2. Run the `provision` script. This will ensure full disk encryption is enabled
   with FileVault, the Xcode command line developer tools are installed,
   Homebrew is installed, Ansible is installed using Homebrew, and run the
   `playbook.yml` playbook.

## Roles

- `hostname`: set the hostname.
- `firewall`: configure the firewall to block incoming connections.
- `defaults`: configure macOS user defaults.
- `homebrew`: tap Homebrew formula repositories, install formulae and casks,
  and start and register Homebrew services to launch at login.
- `python`: install poetry and pipx.

## Manual configuration

The following settings are not managed with Ansible, and must be set manually:

- System Preferences → General → Recent Items: set to None.
- System Preferences → General → Allow Handoff between this Mac and your iCloud
  devices: untick.
- System Preferences → Security & Privacy → General: require password 5
  seconds after sleep or screensaver begins.
- System Preferences → Spotlight → Search Results: untick Spotlight
  Suggestions.
- System Preferences → Spotlight → Search Results: untick Allow Spotlight
  Suggestions in Look up.
- System Preferences → Displays → Night Shift: set Schedule to Sunset to
  Sunrise.
- System Preferences → Displays → Night Shift: set Colour Temperature to More
  Warm.
- System Preferences → Keyboard → Keyboard → Modifier Keys...: set Caps Lock
  Key to Control.
- System Preferences → Keyboard → Text: remove default replacement.
- System Preferences → Software Update → Automatically keep my Mac up to date.
- System Preferences → Network → Wi-Fi → Advanced → DNS → DNS Servers: change
  DNS servers to Cloudflare's 1.1.1.1 and 1.0.0.1.
