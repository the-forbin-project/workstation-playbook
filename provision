#!/bin/bash

set -euo pipefail

# Full disk encryption.
if ! fdesetup isactive > /dev/null; then
  echo "Enabling full disk encryption. Re-run provisioning after rebooting."
  sudo fdesetup enable -user "$LOGNAME"
  exit
fi

# Xcode command line developer tools.
if [[ ! -d /Library/Developer/CommandLineTools ]]; then
  echo "Installing Xcode command line developer tools. Re-run provisioning after installation."
  xcode-select --install
  exit
fi

# Homebrew.
export HOMEBREW_NO_ANALYTICS=1
export HOMEBREW_NO_EMOJI=1
export HOMEBREW_NO_GITHUB_API=1
export HOMEBREW_NO_INSECURE_REDIRECT=1

if [[ ! -d /usr/local/Homebrew ]]; then
  echo "Installing Homebrew."
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  brew update
  brew analytics off
fi

if ! command -v ansible > /dev/null; then
  echo "Installing Ansible with Homebrew."
  brew install ansible
fi

echo "Provisioning the system with Ansible."
ansible-playbook --ask-become-pass playbook.yml
